#import "carraywrapper.H"

@implementation Carr_int8_t
@synthesize address;
@synthesize x;
@synthesize y;
@synthesize z;
-(int8_t)getAtX:(size_t)xindex
{
	return self.address[xindex];
}
-(int8_t*)addrOfX:(size_t)xindex
{
	return self.address + xindex;
}
-(void)setAtX:(size_t)xindex input:(int8_t)input
{
	self.address[xindex] = input;
}
-(int8_t)getAtY:(size_t)yindex yX:(size_t)xindex
{
	return self.address[(yindex * self.x) + xindex];
}
-(int8_t*)addrOfY:(size_t)yindex yX:(size_t)xindex
{
	return self.address + (yindex * self.x) + xindex;
}
-(void)setAtY:(size_t)yindex yX:(size_t)xindex input:(int8_t)input
{
	self.address[(yindex * self.x) + xindex] = input;
}
-(int8_t)getAtZ:(size_t)zindex zY:(size_t)yindex zyX:(size_t)xindex
{
	return self.address[(zindex * self.y) + (yindex * self.x) + xindex];
}
-(int8_t*)addrOfZ:(size_t)zindex zY:(size_t)yindex zyX:(size_t)xindex
{
	return self.address + (zindex * self.y) + (yindex * self.x) + xindex;
}
-(void)setAtZ:(size_t)zindex zY:(size_t)yindex zyX:(size_t)xindex input:(int8_t)input
{
	self.address[(zindex * self.y) + (yindex * self.x) + xindex] = input;
}
-(int8_t)getAtW:(size_t)windex wZ:(size_t)zindex wzY:(size_t)yindex wzyX:(size_t)xindex
{
	return self.address[(windex * self.z) + (zindex * self.y) + (yindex * self.x) + xindex];
}
-(int8_t*)addrOfW:(size_t)windex wZ:(size_t)zindex wzY:(size_t)yindex wzyX:(size_t)xindex
{
	return self.address + (windex * self.z) + (zindex * self.y) + (yindex * self.x) + xindex;
}
-(void)setAtW:(size_t)windex wZ:(size_t)zindex wzY:(size_t)yindex wzyX:(size_t)xindex input:(int8_t)input
{
	self.address[(windex * self.z) + (zindex * self.y) + (yindex * self.x) + xindex] = input;
}

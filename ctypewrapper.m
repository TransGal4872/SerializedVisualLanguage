#import "ctypewrapper.H"

@implementation C_int8_t
@synthesize i;
@end

@implementation C_int16_t
@synthesize i;
@end

@implementation C_int32_t
@synthesize i;
@end

@implementation C_int64_t
@synthesize i;
@end

@implementation C_uint8_t
@synthesize ui;
@end

@implementation C_uint16_t
@synthesize ui;
@end

@implementation C_uint32_t
@synthesize ui;
@end

@implementation C_uint64_t
@synthesize ui;
@end

@implementation C_float
@synthesize f;
@end

@implementation C_double
@synthesize f;
@end

@implementation Cptr_int8_t
@synthesize address;
-(int8_t)getAt:(size_t)index
{
	return self.address[index];
}
-(int8_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(int8_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_int16_t
@synthesize address;
-(int16_t)getAt:(size_t)index
{
	return self.address[index];
}
-(int16_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(int16_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_int32_t
@synthesize address;
-(int32_t)getAt:(size_t)index
{
	return self.address[index];
}
-(int32_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(int32_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_int64_t
@synthesize address;
-(int64_t)getAt:(size_t)index
{
	return self.address[index];
}
-(int64_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(int64_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_uint8_t
@synthesize address;
-(uint8_t)getAt:(size_t)index
{
	return self.address[index];
}
-(uint8_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(uint8_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_uint16_t
@synthesize address;
-(uint16_t)getAt:(size_t)index
{
	return self.address[index];
}
-(uint16_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(uint16_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_uint32_t
@synthesize address;
-(uint32_t)getAt:(size_t)index
{
	return self.address[index];
}
-(uint32_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(uint32_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_uint64_t
@synthesize address;
-(uint64_t)getAt:(size_t)index
{
	return self.address[index];
}
-(uint64_t*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(uint64_t)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_float
@synthesize address;
-(float)getAt:(size_t)index
{
	return self.address[index];
}
-(float*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(float)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_double
@synthesize address;
-(double)getAt:(size_t)index
{
	return self.address[index];
}
-(double*)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(double)input
{
	self.address[index] = input;
}
@end

@implementation Cptr_void
@synthesize address;
-(void*)getAt:(size_t)index
{
	return self.address[index];
}
-(void**)addrOf:(size_t)index
{
	return self.address + index;
}
-(void)setAt:(size_t)index input:(void*)input
{
	self.address[index] = input;
}
@end

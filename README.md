Prerequisits:
- Objective C++ compiler
- `flexc++`

This programming language can be used as a language unto itself, but is
intended to also be the backend serialization format for a WYSIWYG
Visual Programming Language which draws edges from pointers to their
source, from gotos to their label (including gotos implicitly created by
control flow statements), and from function calls to their definitions.
The visual language will also use groups for namespaces.

The main feature of this language is that it has built-in support for
multiple return values. This version does not yet include reference
counting, but a future version may be rewritten to use sharedptr. 
